package com.serverapp.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.serverapp.model.Server;


public interface ServerRepo extends JpaRepository<Server,Long>{

	Server findByIpAddress(String ipAddress);
}
