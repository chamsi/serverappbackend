package com.serverapp;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.serverapp.enumration.Status;
import com.serverapp.model.Server;
import com.serverapp.repo.ServerRepo;

@SpringBootApplication
public class ServerappApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerappApplication.class, args);
	}
/*
	@Bean
	CommandLineRunner run (ServerRepo serverRepo){
		return args ->{
			serverRepo.save(new Server(null,"192.168.100.1","linux","16gb","personal pc","http://localhost:8080/server/image/server2.png",Status.SERVER_UP));
			
		};
	}*/
}
